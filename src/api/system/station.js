import request from '@/utils/request'

// 查询充电桩列表
export function listStation(query) {
  return request({
    url: '/system/station/list',
    method: 'get',
    params: query
  })
}

// 查询充电桩详细
export function getStation(stationId) {
  return request({
    url: '/system/station/' + stationId,
    method: 'get'
  })
}

// 新增充电桩
export function addStation(data) {
  return request({
    url: '/system/station',
    method: 'post',
    data: data
  })
}

// 修改充电桩
export function updateStation(data) {
  return request({
    url: '/system/station',
    method: 'put',
    data: data
  })
}

// 删除充电桩
export function delStation(stationId) {
  return request({
    url: '/system/station/' + stationId,
    method: 'delete'
  })
}
